from django.contrib import admin
from .models import Recipe, RecipeStep


# admin.site.register(Recipe)
@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display= ("title","description","created_on","id")


@admin.register(RecipeStep)
class RecipeStepAdmin(admin.ModelAdmin):
    list_display=(
        "recipe_title",
        "order",
        "id",
    )
