from django.shortcuts import render, get_object_or_404,redirect
from .models import Recipe
from .forms import RecipeForm
from django.contrib.auth.decorators import login_required



@login_required
def show_recipe(request, id):
    a_really_cool_recipe = get_object_or_404(Recipe, id=id)
    context = {
        "recipe_object": a_really_cool_recipe
    }
    return render(request, "recipes/detail.html", context)

def list_recipes(request):
    recipes = Recipe.objects.all()
    context = {
        "recipe_list": recipes,
    }
    return render(
        request,
        "recipes/list.html",
        context,
    )
def create_recipe(request):
    if request.method == "POST":
        form = RecipeForm(request.POST)
        if form.is_valid():
            recipe = form.save(False)
            recipe.author = request.user
            recipe.save
            # form.save()
            return redirect("list_recipes")
    else:
        form = RecipeForm()
    context = {"form": form}
    return render(request, "recipes/create.html", context)

def edit_recipe(request, id):
    recipe = Recipe.objects.get(id=id)
    if request.method == "POST":
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()
            return redirect("show_recipe", id=id)
    else:
        form = RecipeForm(instance=recipe)
        context = {
            "recipe_object": recipe,
            "form": form,
        }
        return render(request, "recipes/create.html", context)
