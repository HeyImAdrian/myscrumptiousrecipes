from django.urls import path
from .views import show_recipe,list_recipes,create_recipe,edit_recipe

urlpatterns = [
    path("<int:id>/", show_recipe, name = "show_recipe"),
    path("", list_recipes, name="list_recipes"),
    path("create/", create_recipe, name="create_recipe"),
    path("<int:id>/edit/", edit_recipe, name="edit_recipe"),
]
